module.exports = {
  head: [
    ['meta', { name: 'referrer', content: 'no-referrer' }]
  ],
  plugins: [
    ["vuepress-plugin-auto-sidebar",{}],
    'permalink-pinyin'
  ],
  themeConfig: {
    navbar:false
  }
}