---
autoGroup-0: AC Design
title: 设计价值观
---
![截屏2022-03-25 下午2.35.36.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1648190149678-61c5a3d4-1043-4bee-8f13-7dc130c72dec.png#clientId=ue5c5b78d-077a-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u3a6bbf10&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-25%20%E4%B8%8B%E5%8D%882.35.36.png&originHeight=230&originWidth=1180&originalType=binary&ratio=1&rotation=0&showTitle=false&size=34208&status=done&style=none&taskId=u3c7b5a70-273e-4f5c-be54-f012a3dc30d&title=)
<a name="CcIfZ"></a>
### 流动 – 设计、自然、时光、掠影
流动的科技犹如光影的浓淡、时光的流逝、自然的韵美、设计的张弛，带来全新的思想和机遇，充满未知的挑战，诠释人与空间，人与时间的绝美构思，崇尚开放、包容、专业，变通，代表技术、科技、思想，也代表新的设计价值；
<a name="ssijm"></a>
### 坚定 – 坚定不移的目标感，寻找伙伴，克服困难
产品设计应该坚定的站在用户的视角，更大维度的促成结果的达成，兼顾用户不同的需求，建立健康的合作机制与组织形式，保持设计的严谨而偏执，内在逻辑的自洽。
<a name="lE1zZ"></a>
### 远方 – 无为而设计，为未来
所有人都憧憬未来，科技和思想的进步必定带来不一样的生活，美永远无法被替代，统一体验，基因体现，社交向善，观察和预判设计新的流行趋势方向，新颖、现代、超出预期。
