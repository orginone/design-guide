---
autoGroup-0: AC Design
title: 介绍
---
<a name="KIP7k"></a>
### 什么是AC Design?
![截屏2022-03-25 下午2.09.49.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1648190083418-54171c05-9e4e-4c13-a7e2-8feef888a8ba.png#clientId=ud5da355e-d1fc-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=uf776c0b9&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-25%20%E4%B8%8B%E5%8D%882.09.49.png&originHeight=405&originWidth=897&originalType=binary&ratio=1&rotation=0&showTitle=false&size=68937&status=done&style=none&taskId=ueebb0f7c-1037-4119-981f-7a63eb6b625&title=)<br />AC Design全称AssetCloud Design - 资产云设计系统。<br />为响应党的号召，持续高质量发展建设，实现支撑复杂、庞大的企业级应用产品，应对快速变化的业态和产业布局，响应更加高频的变动及频繁项目并行，高效的协同产品上下游全链路职能岗位，设计师和工程师更密切的配合。同时在经过大量的调研、实践、应用，沉淀出更加横向、通用的控件和组件，通过更加自然的交互体验方式灵活的产品拼搭，实现低门槛、覆盖广的设计系统特性。 
