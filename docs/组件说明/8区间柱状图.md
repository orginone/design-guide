---
autoGroup-6: 图表
title: 区间柱状图
---
支持通过获取数据的最小值和最大值之间的范围，展示不同类目下柱图的数据样式。<br />![RangeColumnChart 区间柱状图.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647226510139-e3497c75-dee5-4181-9c8d-eb87584bb2fb.png#clientId=ufc0380fe-4a83-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u929c6b20&margin=%5Bobject%20Object%5D&name=RangeColumnChart%20%E5%8C%BA%E9%97%B4%E6%9F%B1%E7%8A%B6%E5%9B%BE.png&originHeight=486&originWidth=414&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13859&status=done&style=none&taskId=uaa8c3fc8-8080-48a6-8b83-622616c0ee8&title=)
