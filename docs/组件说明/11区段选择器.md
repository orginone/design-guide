---
autoGroup-4: 数据输入
title: 区段选择器
---
用于在给定的范围内选择一个值。
<a name="xf128"></a>
### 使用指南

- 在数值输入过程中，需要提供实时的可视化数据比例反馈时。

![截屏2022-03-20 下午4.18.38.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647764342310-8787cea0-79c9-41c6-9651-d39bb7bb5898.png#clientId=ub8f52ed5-bdb8-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u8927c4f2&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.18.38.png&originHeight=309&originWidth=325&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17447&status=done&style=none&taskId=u3ec983a0-4998-41a0-bab8-644be39bee5&title=)![截屏2022-03-20 下午4.18.43.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647764342358-c9e54930-26fe-4712-ad75-315cf6bb627f.png#clientId=ub8f52ed5-bdb8-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u02f66ee6&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.18.43.png&originHeight=223&originWidth=474&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16486&status=done&style=none&taskId=ucfe0749e-8b97-43f9-a8c0-5615af36fc0&title=)
