---
autoGroup-4: 数据展示
title: 进度指示器
---
为用户显示操作的当前进度和状态。
<a name="zN4LZ"></a>
### 使用指南

- 当操作需要较长时间才能完成，需要给用户完成任务的预期时
- 当操作需要展示一个完成进度的百分比

![截屏2022-03-20 下午4.41.45.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647765732356-d694d3b0-3629-4555-b108-c86a077ba6ea.png#clientId=uf7a50d6f-8771-4&crop=0&crop=0&crop=1&crop=1&from=ui&height=314&id=u37705bfc&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.41.45.png&originHeight=314&originWidth=478&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17877&status=done&style=none&taskId=u6937fe46-49a0-4962-b412-f3ac045895a&title=&width=478)![截屏2022-03-20 下午4.41.49.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647765732371-57036590-2341-4a97-8de1-9ef2d3b1854b.png#clientId=uf7a50d6f-8771-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u5657e895&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.41.49.png&originHeight=220&originWidth=231&originalType=binary&ratio=1&rotation=0&showTitle=false&size=11614&status=done&style=none&taskId=u6c072f16-65a8-4665-b639-d11311898c1&title=)![截屏2022-03-20 下午4.41.54.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647765732390-0c4103f1-7d46-4acd-ad09-f6f822d18843.png#clientId=uf7a50d6f-8771-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u3b135a44&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.41.54.png&originHeight=204&originWidth=357&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18495&status=done&style=none&taskId=u38c03053-f13a-4a60-b73a-90a5561e5ab&title=)![截屏2022-03-20 下午4.42.00.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647765732399-19a35fd2-c6ec-47c3-88e4-440de1517ad0.png#clientId=uf7a50d6f-8771-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u5d4ac8be&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%884.42.00.png&originHeight=149&originWidth=247&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13287&status=done&style=none&taskId=u347373b1-9fa8-45e2-a261-a2efb431e06&title=)
