---
autoGroup-3: 其他
title: 固钉
---
将页面元素钉在可视范围。
<a name="hcCkf"></a>
### 使用指南

- 当内容区域比较长，需要滚动页面时，这部分内容对应的操作或者导航需要在滚动范围内始终展现。如表单的取消/提交按钮。

![截屏2022-04-01 上午2.04.46.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1648749934588-6a6df617-3b83-4d39-b602-ae9fdfc550aa.png#clientId=u13da3a0f-d565-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u071b922a&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-04-01%20%E4%B8%8A%E5%8D%882.04.46.png&originHeight=307&originWidth=293&originalType=binary&ratio=1&rotation=0&showTitle=false&size=12423&status=done&style=none&taskId=u3fbb2b6f-d9a9-4f01-8057-8efb3405cce&title=)
