---
autoGroup-5: 通用组件
title: 栅格布局组件
---
## 珊格布局组件
<a name="XdMAw"></a>
### 使用指南

- PC first，其他屏幕支持自适应，因此默认情况下 device 处于 desktop 模式，此时 columns 为12列；
- 更改 device 参数为 tablet 后 columns 为8列；
- 更改 device 参数为 phone 后 columns 为4列，同时 Cell会根据内置规则进行自适应调配。

![截屏2022-03-20 下午12.19.47.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647749997523-56b0d90f-1629-4744-988d-1d03068ac9bd.png#clientId=u9d91109b-d460-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=ubc10f130&margin=%5Bobject%20Object%5D&name=%E6%88%AA%E5%B1%8F2022-03-20%20%E4%B8%8B%E5%8D%8812.19.47.png&originHeight=673&originWidth=984&originalType=binary&ratio=1&rotation=0&showTitle=false&size=43655&status=done&style=none&taskId=ud83336d5-c031-422a-8edf-2b95360e60b&title=)
