---
autoGroup-6: 图表
title: 折线图
---
使用一条折线的线段显示数据在一个具有顺序性的维度上的变化。<br />![LineChart 折线图.png](https://cdn.nlark.com/yuque/0/2022/png/477552/1647226147604-0febb7ea-edff-4e78-9aba-460c1751aafd.png#clientId=ueb2c58b0-6f56-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=ude6e3961&margin=%5Bobject%20Object%5D&name=LineChart%20%E6%8A%98%E7%BA%BF%E5%9B%BE.png&originHeight=486&originWidth=414&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18065&status=done&style=none&taskId=uc733fdf6-a7c6-4897-982c-aa3577ba72a&title=)
